///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Oct 25 2020)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/gbsizer.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/valtext.h>
#include <wx/statbmp.h>

#include "Game.h"
///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MyFrame2
///////////////////////////////////////////////////////////////////////////////
class MyFrame2 : public wxFrame
{
	private:
        wxDECLARE_EVENT_TABLE();
        Game *game;
        void set_game();
		wxBitmap get_bitmap(string option);
protected:
		wxStaticText* m_labelRound;
		wxStaticText* m_labelRoundNum;
        wxTextCtrl* m_textRoundNum;
        wxButton* m_buttonStart;

        wxStaticText* m_labelLevel;
        wxButton* m_levelEasy;
        wxButton* m_levelMedium;
        wxButton* m_levelHard;

		wxStaticText* m_playerName;
		wxStaticText* m_choose;
		wxStaticText* m_computerLabel;
        wxStaticText* m_computerLevel;
		wxStaticText* m_predict;
		wxStaticText* m_afterPredict;
		wxStaticText* m_winner;
		wxStaticText* m_stas;
		wxStaticText* m_humanwins;
		wxStaticText* m_computerWins;
		wxStaticText* m_Ties;
		wxStaticText* m_win;
		wxStaticText* m_computerSelected;
		wxStaticText* m_humanScore;
		wxStaticText* m_computerScore;
		wxStaticText* m_ties;
		wxStaticText* m_predictHuman;
		wxButton* m_rock;
		wxButton* m_paper;
		wxButton* m_scissors;
		wxStaticText* m_humanChooses;
		wxStaticText* m_humanSelected;
		wxStaticBitmap* m_humanSelectedBitmap;
		wxStaticBitmap* m_computerSelectedBitmap;

	wxMenuBar* m_menubar;
        wxStatusBar* m_statusBar;
		wxBitmap m_bitmap_rock;
		wxBitmap m_bitmap_paper;
		wxBitmap m_bitmap_scissors;




	// Virtual event handlers, overide them in your derived class
        virtual void OnStartClick( wxCommandEvent& event );
		virtual void OnRockClick( wxCommandEvent& event );
		virtual void OnPaperClick( wxCommandEvent& event );
		virtual void OnScissorClick( wxCommandEvent& event );
        void OnAbout(wxCommandEvent& event);
        void OnExit(wxCommandEvent& event);

        virtual void OnEasyClick( wxCommandEvent& event );
        virtual void OnMediumClick( wxCommandEvent& event );
        virtual void OnHardClick( wxCommandEvent& event );
	public:

		MyFrame2(Game *service, wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(593, 394 ), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL );
		~MyFrame2();

};

