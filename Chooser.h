//
// Created by Thao Ton on 10/8/20.
//

#ifndef R_P_S_GAME_CHOOSER_H
#define R_P_S_GAME_CHOOSER_H

#include "constants.h"

class Chooser {
    public:
        // pure virtual function
        virtual CHOICE make_choice() = 0;
        virtual void save_choice(CHOICE human_choice, CHOICE cp_choice) {}
        virtual CHOICE get_predict_human_choice() { return CHOICE::not_set;}
        virtual void clear_history() {};

};


#endif //R_P_S_GAME_CHOOSER_H
