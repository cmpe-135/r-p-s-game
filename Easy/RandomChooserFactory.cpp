//
// Created by Thao Ton on 11/13/20.
//

#include "RandomChooserFactory.h"
#include "RandomChooser1.h"
#include "RandomChooser2.h"
#include <string>
#include "../ChooserFactory.h"

Chooser* RandomChooserFactory::make_chooser(string which) {
    if (which == "random1") {
        return new RandomChooser1();
    }
    else if (which == "random2") {
        return new RandomChooser2();
    }
    else {
        return ChooserFactory::make_chooser(which);
    }
}