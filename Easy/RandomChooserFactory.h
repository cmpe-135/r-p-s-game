//
// Created by Thao Ton on 11/13/20.
//

#ifndef R_P_S_GAME_RANDOMCHOOSERFACTORY_H
#define R_P_S_GAME_RANDOMCHOOSERFACTORY_H

#include <string>
#include "RandomChooser1.h"
#include "RandomChooser2.h"
#include "../ChooserFactory.h"

class RandomChooserFactory: public ChooserFactory {
public:
    static Chooser* make_chooser (string which);
};

#endif //R_P_S_GAME_RANDOMCHOOSERFACTORY_H
