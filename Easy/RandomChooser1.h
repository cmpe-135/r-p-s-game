//
// Created by Thao Ton on 10/8/20.
//

#ifndef R_P_S_GAME_RANDOMCHOOSER1_H
#define R_P_S_GAME_RANDOMCHOOSER1_H
#include "../Chooser.h"

class RandomChooser1 : public Chooser {
public:
    CHOICE make_choice() override;
};
#endif //R_P_S_GAME_RANDOMCHOOSER1_H