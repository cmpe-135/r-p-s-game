//
// Created by Thao Ton on 11/13/20.
//

#ifndef R_P_S_GAME_RANDOMCHOOSER2_H
#define R_P_S_GAME_RANDOMCHOOSER2_H

#include "../Chooser.h"

class RandomChooser2 : public Chooser {
public:
    CHOICE make_choice() override;
};

#endif //R_P_S_GAME_RANDOMCHOOSER2_H
