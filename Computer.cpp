//
// Created by Thao Ton on 9/25/20.
//

#include "Computer.h"
#include <cstdlib>
#include <math.h>
#include "Easy/RandomChooserFactory.h"
#include "Hard/SmartChooser.h"
#include "Hard/SmartChooserFactory.h"
using namespace std;
//void Computer::random_select() {
//    this->choice = (CHOICE) (rand() % CHOICE::scissors + CHOICE::rock);
//}

Computer::Computer(string type, string option) : Player() {
    if (type == "random") {
        chooser = RandomChooserFactory::make_chooser(option);
    }
    else {
        chooser = SmartChooserFactory::make_chooser(option);
    }
}

Computer::Computer() : Player() {
    chooser = new RandomChooser1();
}

void Computer::make_choice() {
    this->choice = chooser->make_choice();
}

void Computer::save_choice(CHOICE human_choice, CHOICE cp_choice) {
     chooser->save_choice(human_choice, cp_choice);
}

CHOICE Computer::get_predict_human_choice() {
    return chooser->get_predict_human_choice();
}

void Computer::reset() {
    Player::reset();
    chooser->clear_history();

}