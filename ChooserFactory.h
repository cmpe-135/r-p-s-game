//
// Created by Thao Ton on 10/9/20.
//

#ifndef R_P_S_GAME_CHOOSERFACTORY_H
#define R_P_S_GAME_CHOOSERFACTORY_H

#include "Chooser.h"
#include <string>
using namespace std;
class ChooserFactory {
public:
    static Chooser* make_chooser (string which);
};


#endif //R_P_S_GAME_CHOOSERFACTORY_H
