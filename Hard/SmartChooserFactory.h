//
// Created by Thao Ton on 11/13/20.
//

#ifndef R_P_S_GAME_SMARTCHOOSERFACTORY_H
#define R_P_S_GAME_SMARTCHOOSERFACTORY_H

#include "../ChooserFactory.h"

class SmartChooserFactory: public ChooserFactory {
public:
    static Chooser* make_chooser (string which);
};


#endif //R_P_S_GAME_SMARTCHOOSERFACTORY_H
