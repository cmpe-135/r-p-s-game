//
// Created by Thao Ton on 10/8/20.
//

#ifndef R_P_S_GAME_SMARTCHOOSER_H
#define R_P_S_GAME_SMARTCHOOSER_H
#include <unordered_map>
#include "../Chooser.h"
#include <string>
using namespace std;

class SmartChooser : public Chooser {
public:
    SmartChooser();
    CHOICE make_choice() override;
    void save_choice(CHOICE human_choice, CHOICE cp_choice) override;
    void print_map(std::unordered_map<string,int> const &m);
    void clear_history() override;
    CHOICE get_predict_human_choice() override;
private:
    string current_sequence;
    unordered_map <string, int> historical_move;
    CHOICE predict_human_choice;
};
#endif //R_P_S_GAME_SMARTCHOOSER_H
