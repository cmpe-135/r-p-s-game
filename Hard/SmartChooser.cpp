//
// Created by Thao Ton on 10/8/20.
//
#include <iostream>
#include "SmartChooser.h"


SmartChooser::SmartChooser() {
    predict_human_choice = CHOICE::not_set;
}

CHOICE SmartChooser::make_choice() {


    cout << "Print current sequence " << current_sequence << endl;

    this->print_map(historical_move);

    string found_sequence = "";
    int max = -1;
    if (current_sequence.length() > 4) {
        string sequence = current_sequence.substr(current_sequence.length() -4,4);
        for (pair<string, int> kv : historical_move){
            if (kv.first.substr(0,4) == sequence) {
               if(kv.second > max) {
                   max = kv.second;
                   found_sequence = kv.first;
               }
            }

        }
    }

    if (max > 0) {
        CHOICE human_choice = (CHOICE) int(found_sequence[found_sequence.length() - 1] - '0');
        predict_human_choice = human_choice;

//        cout << "found sequence " << found_sequence << " with value " << max << " human choice " << human_choice << endl;

        if (human_choice == CHOICE::rock) {
            return CHOICE::paper;
        }
        else if (human_choice == CHOICE::paper) {
            return CHOICE::scissors;
        }
        else {
            return CHOICE::rock;
        }
    }
    else {
        predict_human_choice = CHOICE::not_set; //reset predict_option when making random choice
        return  (CHOICE) (rand() % CHOICE::scissors + CHOICE::rock);
    }
}
void SmartChooser::save_choice(CHOICE human_choice, CHOICE cp_choice) {
    current_sequence = current_sequence + to_string(human_choice) + to_string(cp_choice);
    if (current_sequence.length() > 5) {
        string sequence = current_sequence.substr(current_sequence.length() -6,5);
        if (historical_move.find(sequence) == historical_move.end()) { // not present
            historical_move.insert({sequence, 1});
        }
        else {
            historical_move[sequence]++;
        }
    }

}


void SmartChooser::print_map(std::unordered_map<string,int> const &m)
{
    for (auto const& pair: m) {
        std::cout << "{" << pair.first << ": " << pair.second << "}\n";
    }
}

CHOICE SmartChooser::get_predict_human_choice() {
    return predict_human_choice;
}

void SmartChooser::clear_history() {
    historical_move.clear();
    current_sequence = "";
    predict_human_choice = CHOICE::not_set;
};
