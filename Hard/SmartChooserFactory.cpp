//
// Created by Thao Ton on 11/13/20.
//

#include "SmartChooserFactory.h"
#include "SmartChooser.h"
Chooser* SmartChooserFactory::make_chooser(string which) {
    if (which == "smart") {
        return new SmartChooser();
    }
    else {
        return ChooserFactory::make_chooser(which) ;
    }
}