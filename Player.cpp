//
// Created by Thao Ton on 9/25/20.
//

#include "Player.h"


Player::Player() {
    this->score = 0;
    this->choice = CHOICE::not_set;
}
void Player::set_choice(CHOICE choice) {
    this->choice = choice;
}
void Player::set_score(int score) {
    this->score = score;
}
CHOICE Player::get_choice() {
    return choice;
}
int Player::get_score() {
    return score;
}

string Player::get_choice_string() {
    if (this->choice == CHOICE::rock) {
        return "rock";
    }
    if (this->choice == CHOICE::paper) {
        return "paper";
    }
    if (this->choice == CHOICE::scissors) {
        return "scissors";
    }
    return "";
}

void Player::reset() {
    this->choice = CHOICE::not_set;
    this->score = 0;
};