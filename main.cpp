#include <iostream>
#include <string>
#include <fstream>
#include "constants.h"
#include "HumanPlayer.h"
#include "Computer.h"
#include <vector>

using namespace std;
int compare_result(CHOICE choice1, CHOICE choice2);
int compare_result(CHOICE choice1, CHOICE choice2){
    // choice 1 > choice2 => result = 1
    int result = 0;
    if ((choice1 == CHOICE::paper && choice2 == CHOICE::rock) ||
        (choice1 == CHOICE::rock && choice2 == CHOICE::scissors) ||
        (choice1 == CHOICE::scissors && choice2 == CHOICE::paper))  {
        result = 1;
    }
    else if ((choice2 == CHOICE::paper && choice1 == CHOICE::rock) ||
             (choice2 == CHOICE::rock && choice1 == CHOICE::scissors) ||
             (choice2 == CHOICE::scissors && choice1 == CHOICE::paper)) {
        result = -1;
    }
    return result;
}

int main(int argc, char **argv) {
    int round = 0;
    int choice;
    string name = "thao";
    string option = "random";
    int count = 0;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-m") == 0 ) {
            option = "smart";
        }
    }


    HumanPlayer humanPlayer = HumanPlayer(); // Create new humaneplayer object



//    cout << "Enter your name: " ;
//    cin>> name;
//    humanPlayer.set_name(name);
//
//    cout<< "Please enter number of rounds (less than 20 rounds):" << endl;
//    cin >> round;
//
//    cout<< "Please enter computer's option:" << endl;
//    cin >> option;

    Computer computer = Computer(option);

    string line;
    ifstream i_file ("/Users/thaoton/Desktop/r-p-s-game/input/choices2.txt");
    vector<CHOICE> choices_input;

    if (i_file.is_open()) {
        while (getline(i_file, line))
        {
            choices_input.push_back((CHOICE) stoi(line));
        }
    }
    round = choices_input.size();

    i_file.close();


    while (count <= round) {
        cout << "\n\n########################################" << endl;
        cout << "round: " << count << endl;

        // Select your choice
        cout<<"Please select Rock(" << CHOICE::rock <<") Paper (" << CHOICE::paper <<") Scissors (" << CHOICE::scissors << ")" << endl;



//        input_stream >> choice;
        choice = choices_input[count];

        while (choice > CHOICE::scissors || choice < CHOICE::rock) {
            cout << "Invalid option" << endl;
            cout<<"Please select Rock(" << CHOICE::rock <<") Paper (" << CHOICE::paper <<") Scissors (" << CHOICE::scissors << ")" << endl;
//            input_stream >> choice;
            choice = choices_input[count];

        }

        humanPlayer.set_choice((CHOICE)choice);
        computer.make_choice();
        int result = compare_result(humanPlayer.get_choice(), computer.get_choice());
        cout << humanPlayer.get_name() << "human selects " << humanPlayer.get_choice_string() << " Computer selects " << computer.get_choice_string() << endl;

        if (result == 0) {
            cout << "Tie" << endl;
        }
        else if (result > 0) {
            cout << humanPlayer.get_name() << "human won." << endl;
            humanPlayer.set_score(humanPlayer.get_score() + 1);

        }
        else {
            cout << "Computer won." << endl;
            computer.set_score(computer.get_score() + 1);
        };
        computer.save_choice(humanPlayer.get_choice(), computer.get_choice());

        count++;
    }
    cout << "---------------" << endl;
    int computer_score = computer.get_score();
    int human_score = humanPlayer.get_score();

    cout << "Computer score: " <<  computer.get_score() << endl;
    cout << "Human score: " << humanPlayer.get_score() << endl;
        if (computer_score > human_score) {
            cout << "Computer won." << endl;
        }
        else if (computer_score < human_score) {
            cout << "Human won." << endl;
        }
        else {
            cout << "Tie." << endl;
        }

}