//
// Created by Thao Ton on 10/25/20.
//

#ifndef R_P_S_GAME_GAME_H
#define R_P_S_GAME_GAME_H
#include <iostream>
#include <string>
#include <fstream>
#include "constants.h"
#include "HumanPlayer.h"
#include "Computer.h"
#include <vector>
class Game {
private:
    int currentRound;
    int totalRound;
    HumanPlayer humanPlayer;
    Computer computer;
    int compare_result(CHOICE choice1, CHOICE choice2);



public:
    Game();
    void start_game(int totalRound);
    string winner();
    string human_select();
    string computer_select();
    string computer_predict_human();
    string human_score();
    string computer_score();
    string tie_score();
    string get_current_round();
    string convert_choice(CHOICE choice);
    void setComputer(string type, string option);
    void play_game(CHOICE option);
    bool is_gameover();
    bool is_game_start();
};
#endif //R_P_S_GAME_GAME_H
