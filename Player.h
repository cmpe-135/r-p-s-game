//
// Created by Thao Ton on 9/25/20.
//

#ifndef R_P_S_GAME_PLAYER_H
#define R_P_S_GAME_PLAYER_H

#include "constants.h"
#include <string>
using namespace std;

class Player {
protected:
    CHOICE choice;
    int score;
public:
    Player();
    void set_choice(CHOICE choice);
    void set_score(int score);
    CHOICE get_choice();
    int get_score();
    string get_choice_string();
    void reset();

};


#endif //R_P_S_GAME_PLAYER_H
