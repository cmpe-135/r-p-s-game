//
// Created by Thao Ton on 9/25/20.
//

#ifndef R_P_S_GAME_HUMANPLAYER_H
#define R_P_S_GAME_HUMANPLAYER_H

#include "Player.h"
using namespace std;
#include <string>

class HumanPlayer: public Player {
private:
    string name;
public:
    void set_name (string name);
    string get_name();
};


#endif //R_P_S_GAME_HUMANPLAYER_H
