//
// Created by Thao Ton on 9/25/20.
//

#ifndef R_P_S_GAME_CONSTANTS_H
#define R_P_S_GAME_CONSTANTS_H

enum CHOICE: int {
    rock = 0,
    paper = 1,
    scissors = 2,
    not_set = 3,
};


#endif //R_P_S_GAME_CONSTANTS_H
