///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.9.0 Oct 25 2020)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "UI.h"
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include<wx/wx.h>
#endif

#define MAX_ROUND 100

class MyApp: public wxApp
{
public:
    virtual bool OnInit();
};

bool MyApp::OnInit(){
    MyFrame2 *frame = new MyFrame2(
                                    new Game(),
                                    NULL,
                                   NULL,
                                   "Rock Paper Scissors",
                                   wxPoint(50,50),
                                   wxSize(600,700),
                                   wxDEFAULT_FRAME_STYLE
    );

//    frame->setService(new Game());
    frame->Show(true);
    return true;
}
void MyFrame2::OnRockClick( wxCommandEvent& event ) // { event.Skip(); }
{
    game->play_game(CHOICE::rock);
    set_game();
}
void MyFrame2::OnPaperClick( wxCommandEvent& event ) // { event.Skip(); }
{
    game->play_game(CHOICE::paper);
    set_game();
}
void MyFrame2::OnScissorClick( wxCommandEvent& event ) // { event.Skip(); }
{
    game->play_game(CHOICE::scissors);
    set_game();
}

void MyFrame2::OnEasyClick( wxCommandEvent& event ) // { event.Skip(); }
{
    m_computerLevel->SetLabelText("Easy ");
    game->setComputer("random", "random1");
}
void MyFrame2::OnMediumClick( wxCommandEvent& event ) // { event.Skip(); }
{
    m_computerLevel->SetLabelText("Medium ");
    game->setComputer("random", "random2");
}
void MyFrame2::OnHardClick( wxCommandEvent& event ) // { event.Skip(); }
{
    m_computerLevel->SetLabelText("Hard ");
    game->setComputer("smart", "smart");
}

void MyFrame2::OnStartClick( wxCommandEvent& event ) {
    int round = wxAtoi(m_textRoundNum->GetValue());
    if (round > 0) {

        if (round <= MAX_ROUND) {
            game->start_game(round);
            m_rock->Enable();
            m_paper->Enable();
            m_scissors->Enable();
//        m_buttonStart->Disable();
            wxString mystring = wxString::Format(wxT("Rock Paper Scissors: %d rounds/game"), round);
            SetTitle(mystring);
            set_game();
        }
        else {
            wxMessageBox(wxString::Format(
                    "Please enter a round number that is less than %d\n",
                   MAX_ROUND),
                         "Alert",
                         wxOK | wxICON_INFORMATION,
                         this);
        }


    }
    set_game();
}


void MyFrame2::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox(wxString::Format(
            "This is a Rock Paper Scissors game\n"
            "built with %s\n"
            "and running under %s.",
            wxVERSION_STRING,
            wxGetOsDescription()
                 ),
                 "About the RPS game",
                 wxOK | wxICON_INFORMATION,
                 this);
}

void MyFrame2::OnExit(wxCommandEvent& WXUNUSED(event))
{
    Close(true);  // true is to force the frame to close
}

enum {
    ID_RockButtonClick,
    ID_PaperButtonClick,
    ID_ScissorButtonClick,
    ID_Start,
    ID_About,
    ID_Exit,
    ID_EasyButtonClick,
    ID_MediumButtonClick,
    ID_HardButtonClick,
};

wxBEGIN_EVENT_TABLE(MyFrame2, wxFrame)
    EVT_MENU(ID_RockButtonClick, MyFrame2::OnRockClick)
    EVT_MENU(ID_PaperButtonClick, MyFrame2::OnPaperClick)
    EVT_MENU(ID_ScissorButtonClick, MyFrame2::OnScissorClick)
    EVT_MENU(ID_Start,  MyFrame2::OnStartClick)
    EVT_MENU(ID_About, MyFrame2::OnAbout)
    EVT_MENU(ID_Exit,  MyFrame2::OnExit)

    EVT_MENU(ID_EasyButtonClick, MyFrame2::OnEasyClick)
    EVT_MENU(ID_MediumButtonClick, MyFrame2::OnMediumClick)
    EVT_MENU(ID_HardButtonClick, MyFrame2::OnHardClick)

wxEND_EVENT_TABLE()

wxIMPLEMENT_APP(MyApp);
/////////////////////////////////////////////////////////////////////

wxBitmap MyFrame2::get_bitmap(string option) {
    if (option == "Rock") {
        return m_bitmap_rock;
    }
    else if (option == "Paper") {
        return m_bitmap_paper;
    }
    else if (option == "Scissors") {
        return m_bitmap_scissors;
    }
    else {
        return  wxBitmap( );
    }
}

void MyFrame2::set_game(){
    m_labelRoundNum->SetLabel(game->get_current_round());
    m_humanScore->SetLabel(game->human_score());
    m_computerScore->SetLabel(game->computer_score());
    m_ties->SetLabel(game->tie_score());
//    m_humanSelected->SetLabel(game->human_select());

    m_humanSelectedBitmap->SetBitmap(get_bitmap(game->human_select()));

    m_predictHuman->SetLabel(game->computer_predict_human());
    //m_computerSelected->SetLabel(game->computer_select());

    m_computerSelectedBitmap->SetBitmap(get_bitmap(game->computer_select()));
    m_win->SetLabel(game->winner());

//    m_labelRoundNum->InvalidateBestSize();
    if (!game->is_game_start()) {
        m_levelEasy->Disable();
        m_levelMedium->Disable();
        m_levelHard->Disable();
    }
    else {
        m_levelEasy->Enable();
        m_levelMedium->Enable();
        m_levelHard->Enable();
    }

    if (game->is_gameover()) {
        m_rock->Disable();
        m_paper->Disable();
        m_scissors->Disable();

        m_buttonStart->Enable();
        m_buttonStart->SetLabel("Start");
    }
    else {
        m_buttonStart->SetLabel("Restart");
    }
    this->Layout();

}

MyFrame2::MyFrame2(Game *service, wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame(parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
    this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHT ) );
    wxImage::AddHandler(new wxPNGHandler);

	wxGridBagSizer* gbSizer2;
	gbSizer2 = new wxGridBagSizer( 0, 0 );
	gbSizer2->SetFlexibleDirection( wxBOTH );
	gbSizer2->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

    m_labelRound = new wxStaticText( this, wxID_ANY, wxT("Round:"), wxDefaultPosition, wxDefaultSize, 0 );
    m_labelRound->Wrap( -1 );
    m_labelRound->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add( m_labelRound, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_labelRoundNum = new wxStaticText( this, wxID_ANY, wxT("100"), wxDefaultPosition, wxDefaultSize, 0 );
    m_labelRoundNum->Wrap( -1 );
    gbSizer2->Add( m_labelRoundNum, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

    m_textRoundNum = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_textRoundNum, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_buttonStart = new wxButton( this, wxID_ANY, wxT("Start"), wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_buttonStart, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_labelLevel = new wxStaticText( this, wxID_ANY, wxT("Level:"), wxDefaultPosition, wxDefaultSize, 0 );
    m_labelLevel->Wrap( -1 );
    m_labelLevel->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add( m_labelLevel, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_levelEasy = new wxButton( this, wxID_ANY, wxT("Easy"), wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_levelEasy, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_levelMedium = new wxButton( this, wxID_ANY, wxT("Medium"), wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_levelMedium, wxGBPosition( 1, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_levelHard = new wxButton( this, wxID_ANY, wxT("Hard"), wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_levelHard, wxGBPosition( 1, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_playerName = new wxStaticText( this, wxID_ANY, wxT("HumanPlayer "), wxDefaultPosition, wxDefaultSize, 0 );
	m_playerName->Wrap( -1 );
    m_playerName->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add( m_playerName, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_choose = new wxStaticText( this, wxID_ANY, wxT("Choose: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_choose->Wrap( -1 );
	gbSizer2->Add( m_choose, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

    m_computerLabel = new wxStaticText(this, wxID_ANY, wxT("Computer"), wxPoint(-2, -2 ), wxDefaultSize, 0 );
	m_computerLabel->Wrap(-1 );
    m_computerLabel->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add(m_computerLabel, wxGBPosition(6, 0 ), wxGBSpan(1, 1 ), wxALL|wxEXPAND, 5 );

    m_computerLevel = new wxStaticText(this, wxID_ANY, wxT("Easy "), wxPoint(-2, -2 ), wxDefaultSize, 0 );
    m_computerLevel->Wrap(-1 );
    m_computerLevel->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add(m_computerLevel, wxGBPosition(6, 2 ), wxGBSpan(1, 1 ), wxALL|wxEXPAND, 5 );


    m_predict = new wxStaticText( this, wxID_ANY, wxT("Predicted human choice: "), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_predict->Wrap( -1 );
	gbSizer2->Add( m_predict, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_afterPredict = new wxStaticText( this, wxID_ANY, wxT("Therefore computer chooses: "), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_afterPredict->Wrap( -1 );
	gbSizer2->Add( m_afterPredict, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_winner = new wxStaticText( this, wxID_ANY, wxT("The winner: "), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_winner->Wrap( -1 );
    m_winner->SetFont( wxFont( 18, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("Times New Roman") ) );
    gbSizer2->Add( m_winner, wxGBPosition( 10, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_stas = new wxStaticText( this, wxID_ANY, wxT("Statistics"), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_stas->Wrap( -1 );
    m_stas->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_SCRIPT, wxFONTSTYLE_ITALIC, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );
    gbSizer2->Add( m_stas, wxGBPosition( 12, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_humanwins = new wxStaticText( this, wxID_ANY, wxT("Human wins: "), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_humanwins->Wrap( -1 );
	gbSizer2->Add( m_humanwins, wxGBPosition( 13, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_computerWins = new wxStaticText( this, wxID_ANY, wxT("Computer wins: "), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_computerWins->Wrap( -1 );
	gbSizer2->Add( m_computerWins, wxGBPosition( 14, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_Ties = new wxStaticText( this, wxID_ANY, wxT("Ties:"), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_Ties->Wrap( -1 );
	gbSizer2->Add( m_Ties, wxGBPosition( 15, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_win = new wxStaticText( this, wxID_ANY, wxT("Computer"), wxPoint( -2,-2 ), wxDefaultSize, 0 );
	m_win->Wrap( -1 );
	gbSizer2->Add( m_win, wxGBPosition( 10, 2 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

//	m_computerSelected = new wxStaticText( this, wxID_ANY, wxT("Scissors"), wxPoint( -2,-2 ), wxDefaultSize, 0 );
//	m_computerSelected->Wrap( -1 );
//    m_computerSelected->SetFont( wxFont( 13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("American Typewriter") ) );
//
    m_computerSelectedBitmap = new wxStaticBitmap(this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_computerSelectedBitmap, wxGBPosition( 8, 2 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

    m_humanScore = new wxStaticText(this, wxID_ANY, wxT("2"), wxPoint(-2, -2 ), wxDefaultSize, 0 );
	m_humanScore->Wrap(-1 );
	gbSizer2->Add(m_humanScore, wxGBPosition(13, 2 ), wxGBSpan(1, 1 ), wxALL|wxEXPAND, 5 );

    m_computerScore = new wxStaticText(this, wxID_ANY, wxT("3"), wxPoint(-2, -2 ), wxDefaultSize, 0 );
	m_computerScore->Wrap(-1 );
	gbSizer2->Add(m_computerScore, wxGBPosition(14, 2), wxGBSpan(1, 1 ), wxALL|wxEXPAND, 5 );

    m_ties = new wxStaticText(this, wxID_ANY, wxT("1"), wxPoint(-2, -2 ), wxDefaultSize, 0 );
	m_ties->Wrap(-1 );
	gbSizer2->Add(m_ties, wxGBPosition(15, 2 ), wxGBSpan(1, 1 ), wxALL|wxEXPAND, 5 );

	m_predictHuman = new wxStaticText( this, wxID_ANY, wxT(""), wxPoint( -1,-1 ), wxDefaultSize, 0 );
	m_predictHuman->Wrap( -1 );
	gbSizer2->Add( m_predictHuman, wxGBPosition( 7, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	m_rock = new wxButton( this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(100,100), 0 );
	m_bitmap_rock = wxBitmap( wxImage("icons/rock.png", wxBITMAP_TYPE_ANY), -1, 1 );
    m_rock->SetBitmap(m_bitmap_rock);
	gbSizer2->Add( m_rock, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_paper = new wxButton( this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(100,100), 0 );
    m_bitmap_paper = wxBitmap( wxImage("icons/paper.png", wxBITMAP_TYPE_ANY), -1, 1 );
    m_paper->SetBitmap( m_bitmap_paper);
	gbSizer2->Add( m_paper, wxGBPosition( 3, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_scissors = new wxButton( this, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(100,100), 0 );
	m_bitmap_scissors = wxBitmap( wxImage("icons/scissors.png", wxBITMAP_TYPE_ANY), -1, 1 );
    m_scissors->SetBitmap( m_bitmap_scissors);
    gbSizer2->Add( m_scissors, wxGBPosition( 3, 3 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_humanChooses = new wxStaticText( this, wxID_ANY, wxT("Human chooses: "), wxDefaultPosition, wxDefaultSize, 0 );
	m_humanChooses->Wrap( -1 );
    gbSizer2->Add( m_humanChooses, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

//	m_humanSelected = new wxStaticText( this, wxID_ANY, wxT("Paper"), wxDefaultPosition, wxDefaultSize, 0 );
//	m_humanSelected->Wrap( -1 );
//    m_humanSelected->SetFont( wxFont( 12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxT("American Typewriter") ) );
//    gbSizer2->Add( m_humanSelected, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

    m_humanSelectedBitmap = new wxStaticBitmap( this, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxDefaultSize, 0 );
    gbSizer2->Add( m_humanSelectedBitmap, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );


	this->SetSizer( gbSizer2 );
	this->Layout();


    wxMenu *fileMenu = new wxMenu;
    fileMenu->Append(ID_Exit,  "E&xit\tAlt-X", "Quit program");
    fileMenu->Append(ID_Start,  "Start/Restart", "Start");

    wxMenu *helpMenu = new wxMenu();
    helpMenu->Append(ID_About, "&About\tF1", "Show about dialog");

    m_menubar = new wxMenuBar();
    m_menubar->Append(fileMenu, "&File");
    m_menubar->Append(helpMenu, "&Help");
    this->SetMenuBar(m_menubar);

    m_statusBar = this->CreateStatusBar( 1, wxSTB_SIZEGRIP, wxID_ANY );
	this->Centre( wxBOTH );

    m_buttonStart->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnStartClick ), NULL, this );
    m_rock->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnRockClick ), NULL, this );
	m_paper->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnPaperClick ), NULL, this );
	m_scissors->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnScissorClick ), NULL, this );

    m_levelEasy->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnEasyClick), NULL, this );
    m_levelMedium->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnMediumClick), NULL, this );
    m_levelHard->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnHardClick), NULL, this );
	this->game = service;

    m_statusBar->PushStatusText("Welcome to Rock Paper Scissors game!");
    m_textRoundNum->SetValue("20");




    // click start button programatically
     wxCommandEvent commandEvent(ID_Start);
    this->OnStartClick(commandEvent);

}

MyFrame2::~MyFrame2()
{
	// Disconnect Events
    m_buttonStart->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnStartClick ), NULL, this );
    m_rock->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnRockClick ), NULL, this );
	m_paper->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnPaperClick ), NULL, this );
	m_scissors->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnScissorClick ), NULL, this );

//    m_levelEasy->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnEasyClick), NULL, this );
//    m_levelMedium->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnMediumClick), NULL, this );
//    m_levelHard->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MyFrame2::OnHardClick), NULL, this );

}
