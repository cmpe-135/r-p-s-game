* Prerequirements
   
     Install cmake
       ```
       brew install cmake #MacOS
       ```
       
     Follow https://github.com/wxFormBuilder/wxFormBuilder to install wxwidget for your operating system, For example,
           
     * In Macos
       ```
           brew install wxmac boost dylibbundler make
       
       ```
   
    To compile the project
    ```
       #this will generate the Make file
       cmake build . 
       #this will generate the run binary file R_P_S_Game
       make 
    ```
    
    To run the project
    ```asm
       ./R_P_S_Game
   ```