//
// Created by Thao Ton on 9/25/20.
//

#ifndef R_P_S_GAME_COMPUTER_H
#define R_P_S_GAME_COMPUTER_H

#include "Player.h"
#include "constants.h"
#include "Chooser.h"
class Computer: public Player {
private:
    Chooser *chooser;

public:
    Computer(string type, string option);
    Computer();
    //void random_select();
    void make_choice();
    void save_choice(CHOICE human_choice, CHOICE cp_choice);
    CHOICE get_predict_human_choice();
    void reset();
};


#endif //R_P_S_GAME_COMPUTER_H
