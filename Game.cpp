//
// Created by Thao Ton on 10/25/20.
//
#include <iostream>
#include <string>
#include <fstream>
#include "constants.h"
#include "HumanPlayer.h"
#include "Computer.h"
#include <vector>
#include "Game.h"

Game::Game() {
    this->totalRound = 0;
    this->currentRound = 0;
    humanPlayer = HumanPlayer();
    humanPlayer.set_name("HUMAN");
    computer = Computer("", "");
}
void Game::setComputer(string type, string option) {
    computer = Computer(type, option);
}

void Game::start_game(int totalRound)
{
    this->totalRound = totalRound;
    this->currentRound = 0;
    humanPlayer.reset();
    computer.reset();
}
string Game::winner() {

    if (currentRound == 0) {
        return "";
    }
    if (currentRound == totalRound && totalRound > 0) {
        if (humanPlayer.get_score() > computer.get_score()) {
            humanPlayer.get_name();
        }
        else if (humanPlayer.get_score() < computer.get_score()) {
            return "COMPUTER";
        } else {
            return "TIE";
        }
    }

    int result = compare_result(humanPlayer.get_choice(), computer.get_choice());
    if (result == 0) {
        return "TIE";
    }
    else if (result > 0) {
        return humanPlayer.get_name();

    }
    else {
        return "COMPUTER";
    };
}

bool Game::is_gameover() {
    return currentRound >= totalRound && totalRound > 0;
}

bool Game::is_game_start() {
    return currentRound == 0;
}

string Game::human_select(){
    CHOICE choice = humanPlayer.get_choice();
    return convert_choice(choice);
}
string Game::computer_select(){
    CHOICE choice = computer.get_choice();
    return convert_choice(choice);
}

string Game::convert_choice(CHOICE choice) {
    if (choice == CHOICE::rock) {
        return "Rock";
    }
    else if (choice == CHOICE::paper) {
        return "Paper";
    }
    else if (choice == CHOICE::scissors) {
        return "Scissors";
    }
    else {
        return " ";
    }
}


string Game::computer_predict_human() {
    CHOICE choice = computer.get_predict_human_choice();
    return convert_choice(choice);
}
string Game::human_score() {
    return to_string(humanPlayer.get_score());
}
string Game::computer_score() {
    return to_string(computer.get_score());
}
string Game::tie_score() {
    return to_string(currentRound - humanPlayer.get_score() - computer.get_score());
}

string Game::get_current_round(){
     return to_string(this->currentRound);
}
void Game::play_game(CHOICE option){
    humanPlayer.set_choice(option);
    computer.make_choice();
    int result = compare_result(humanPlayer.get_choice(), computer.get_choice());
    if (result == 0) {
        cout << "Tie" << endl;
    }
    else if (result > 0) {
        humanPlayer.set_score(humanPlayer.get_score() + 1);
    }
    else {
        computer.set_score(computer.get_score() + 1);
    };
    computer.save_choice(humanPlayer.get_choice(), computer.get_choice());
    currentRound++;
}
int Game::compare_result(CHOICE choice1, CHOICE choice2){
    // choice 1 > choice2 => result = 1
    int result = 0;
    if ((choice1 == CHOICE::paper && choice2 == CHOICE::rock) ||
        (choice1 == CHOICE::rock && choice2 == CHOICE::scissors) ||
        (choice1 == CHOICE::scissors && choice2 == CHOICE::paper))  {
        result = 1;
    }
    else if ((choice2 == CHOICE::paper && choice1 == CHOICE::rock) ||
             (choice2 == CHOICE::rock && choice1 == CHOICE::scissors) ||
             (choice2 == CHOICE::scissors && choice1 == CHOICE::paper)) {
        result = -1;
    }
    return result;
}